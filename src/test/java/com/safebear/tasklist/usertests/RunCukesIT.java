package com.safebear.tasklist.usertests;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(

        // *8. Why isn't this running any of my scenarios?

        plugin = {"pretty", "html:target/cucumber"},
        tags = "~@to-do",
        glue = "com.safebear.tasklist.usertests",
        features = "classpath:tasklist.features/taskmanagement.feature"
)
public class RunCukesIT {


}
