package com.safebear.tasklist.controller;


import com.safebear.tasklist.service.TaskService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@WebMvcTest
@RunWith(SpringRunner.class)
public class TaskRestControllerTest {



    TaskController taskController;

    MockMvc mockMvc;

    // *13. It's complaining that it can't find an implementation of my Task Service interface. How do I stub/mock this out?
    @MockBean
    TaskService taskService;

    @Before
    public void setUp(){


        this.taskController = new TaskController(taskService);
        mockMvc = MockMvcBuilders.standaloneSetup(taskController).build();
    }

    @Test
    public void testTaskList() throws Exception {

        this.mockMvc.perform(MockMvcRequestBuilders.get("/api/tasks"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testSaveTask() throws Exception{

        // *7. This is broken, it's saying that it can't parse my data in the content of the post request - why? I'm sure my JSON is correct.
        this.mockMvc.perform(MockMvcRequestBuilders
                .post("/api/tasks/save")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"taskname\":\"mop the floor\",\"dueDate\":\"05/04/2018\",\"completed\":\"false\"}"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());

    }


}
