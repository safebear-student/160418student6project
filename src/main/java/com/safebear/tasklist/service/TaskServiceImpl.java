package com.safebear.tasklist.service;

import com.safebear.tasklist.model.Task;
import com.safebear.tasklist.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    TaskRepository repository;

    @Override
    public Iterable<Task> list() {

        return this.repository.findAll();
    }

    @Override
    public Task save(Task task) {

        return this.repository.save(task);
    }
}
